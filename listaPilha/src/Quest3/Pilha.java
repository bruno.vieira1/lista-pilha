package Quest3;

import java.util.Scanner;
import java.util.Stack;

import Quest5.Tarefa;

public class Pilha {
	static Scanner sc = new Scanner(System.in);
	static Stack<Tarefa> pilha = new Stack<Tarefa>();

	public static void main(String[] args) {

		operacoes(pilha);

	}

	public static void operacoes(Stack<Tarefa> pilha) {
		Integer op = 0;
		do {
			System.out.println("Informe a opera��o que deseja efetuar:");
			System.out.println(" 1=Inserir tarefa na pilha\n 2=Obter a proxima tarefa da pilha\n 3=Sair.");
			op = sc.nextInt();
			sc.nextLine();

			switch (op) {
			case 1:
				pilha.push(addTarefa());
				System.out.println("Tarefa inserida.");
				break;
			case 2:
				System.out.println(pilha.pop());
				break;
			case 3:
				System.exit(0);
				System.out.println("Obrigado por utilizar nossos servi�os :).");
				break;

			default:
				break;
			}

		} while (true);
	}

	public static Tarefa addTarefa() {
		System.out.println("Informar a tarefa a ser executada: ");
		String tarefa = sc.nextLine();
		return new Tarefa(tarefa);
	}
}