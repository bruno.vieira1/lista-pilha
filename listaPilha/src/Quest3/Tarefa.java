package Quest3;

public class Tarefa {
	String passos;

	public Tarefa() {

	}

	public Tarefa(String passos) {
		super();
		this.passos = passos;
	}

	public String getPassos() {
		return passos;
	}

	public void setPassos(String passos) {
		this.passos = passos;
	}

	@Override
	public String toString() {
		return "Tarefa [passos=" + passos + "]";
	}

}
