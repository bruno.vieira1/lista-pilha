package Quest5;

import java.util.Scanner;
import java.util.Stack;

public class Pilha {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		do {
			System.out.println("Digite 1 para criar a pilha e utilizar o programa:");
			int criar = sc.nextInt();
			if (criar != 1) {
				System.out.println("Voc� n�o digitou 1, tente novamente.");
			} else {
				Stack<Tarefa> pilha = new Stack<Tarefa>();
				System.out.println("A pliha foi criada.");
				operacoes(pilha);
			}
		} while (true);
	}

	public static void operacoes(Stack<Tarefa> pilha) {
		Integer op = 0;
		do {
			System.out.println("Informe a opera��o que deseja efetuar:");
			System.out.println(
					" 2=Adicione tarefas na pilha\n 3=Remova tarefas da pilha\n 4=Indique se a tarefa for conclu�da quando a pilha estiver vazia\n 5=Sair.");
			op = sc.nextInt();
			sc.nextLine();

			switch (op) {
			case 2:
				pilha.push(addTarefa());
				System.out.println("Tarefa criada e armazenada.");
				break;
			case 3:
				pilha.remove(0);
				break;
			case 4:
				if (pilha.size() > 0) {
					System.out.println("H� conte�do na pilha.");
				} else {
					System.out.println("A tarefa foi conclu�da, pois a pilha est� vazia.");
				}
				break;
			case 5:
				System.out.println("Obrigado por utilizar nossos servi�os :)");
				System.exit(0);
				break;

			default:
				break;
			}

		} while (true);
	}

	public static Tarefa addTarefa() {
		System.out.println("Informar a tarefa a ser executada: ");
		String tarefa = sc.nextLine();
		return new Tarefa(tarefa);
	}
}
